package br.com.porta.porta.repositories;

import br.com.porta.porta.models.Porta;
import org.springframework.data.repository.CrudRepository;

public interface PortaRepository extends CrudRepository<Porta, Long> {
}
