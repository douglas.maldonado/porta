package br.com.porta.porta.controllers;

import br.com.porta.porta.models.Porta;
import br.com.porta.porta.services.PortaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/porta")
public class PortaController {

    @Autowired
    private PortaService portaService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Porta createPorta(@RequestBody @Valid Porta porta){

        Porta portaObjeto = portaService.create(porta);

        return portaObjeto;
    }

    @GetMapping("/{id}")
    public Porta getPorta (@PathVariable(name = "id") Long id){
        Porta porta = portaService.getPortaId(id);

        return porta;
    }


}
