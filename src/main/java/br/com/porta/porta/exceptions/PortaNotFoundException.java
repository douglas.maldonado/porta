package br.com.porta.porta.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Porta não cadastrada")
public class PortaNotFoundException extends RuntimeException{

}
