package br.com.porta.porta.models;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
@Table
public class Porta {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "Andar não pode ser em branco")
    @NotNull(message = "Andar não pode ser nulo")
    private String andar;

    @NotBlank(message = "Sala não pode ser em branco")
    @NotNull(message = "Sala não pode ser nulo")
    private String sala;

    public Porta() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAndar() {
        return andar;
    }

    public void setAndar(String andar) {
        this.andar = andar;
    }

    public String getSala() {
        return sala;
    }

    public void setSala(String sala) {
        this.sala = sala;
    }
}
