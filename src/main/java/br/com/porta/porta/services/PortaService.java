package br.com.porta.porta.services;

import br.com.porta.porta.exceptions.PortaNotFoundException;
import br.com.porta.porta.models.Porta;
import br.com.porta.porta.repositories.PortaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PortaService {

    @Autowired
    private PortaRepository portaRepository;

    public Porta create(Porta porta){

        Porta portaObjeto = portaRepository.save(porta);

        return portaObjeto;
    }

    public Porta getPortaId(Long id){

        Optional<Porta> optionalPorta = portaRepository.findById(id);

        if(optionalPorta.isPresent()){
            return optionalPorta.get();
        }

        throw new PortaNotFoundException();

    }

}
